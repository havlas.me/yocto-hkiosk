FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
    file://40-no-autospawn.conf \
    file://40-no-exit-idle-time.conf \
    file://pulseaudio@.service \
"

PACKAGECONFIG ??= "dbus \
    ${@bb.utils.contains('DISTRO_FEATURES', 'bluetooth', 'bluez5', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'zeroconf', 'avahi', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', '3g', 'ofono', '', d)} \
    ${@bb.utils.filter('DISTRO_FEATURES', 'ipv6 systemd x11', d)} \
"

do_install:append () {
    install -d ${D}${sysconfdir}/pulse/client.conf.d
    install -m 644 ${WORKDIR}/40-no-autospawn.conf ${D}${sysconfdir}/pulse/client.conf.d/40-no-autospawn.conf
    install -d ${D}${sysconfdir}/pulse/daemon.conf.d
    install -m 644 ${WORKDIR}/40-no-exit-idle-time.conf ${D}${sysconfdir}/pulse/daemon.conf.d/40-no-exit-idle-time.conf

    if ${@bb.utils.contains('PACKAGECONFIG','systemd','true','false',d)}; then
        install -d ${D}${systemd_system_unitdir}
        install -m 644 ${WORKDIR}/pulseaudio@.service ${D}${systemd_system_unitdir}/pulseaudio@.service
    fi
}

FILES:${PN}-server += " \
    ${sysconfdir}/pulse/client.conf.d \
    ${sysconfdir}/pulse/client.conf.d/40-no-autospawn.conf \
    ${sysconfdir}/pulse/daemon.conf.d \
    ${sysconfdir}/pulse/daemon.conf.d/40-no-exit-idle-time.conf \
    ${systemd_system_unitdir} \
    ${systemd_system_unitdir}/pulseaudio@.service \
"

CONFFILES:pulseaudio-server += "\
    ${sysconfdir}/pulse/client.conf.d/40-no-autospawn.conf \
    ${sysconfdir}/pulse/daemon.conf.d/40-no-exit-idle-time.conf \
"
