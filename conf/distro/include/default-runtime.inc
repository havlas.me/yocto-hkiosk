#
# hKiosk default runtime configuration
#

PREFERRED_PROVIDER_virtual/base-utils ?= "coreutils"
PREFERRED_PROVIDER_virtual/bootloader ?= "u-boot"
PREFERRED_PROVIDER_u-boot-fw-utils ?= "libubootenv"

VIRTUAL-RUNTIME_base-utils ?= "coreutils"
VIRTUAL-RUNTIME_base-utils-hwclock ?= "util-linux-hwclock"
VIRTUAL-RUNTIME_base-utils-syslog ?= ""
VIRTUAL-RUNTIME_syslog ?= ""
