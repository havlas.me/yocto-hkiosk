SUMMARY = "hKiosk base"
LICENSE = "MIT"

PR = "r0"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

VIRTUAL-RUNTIME_python3-gpio ?= ""
VIRTUAL-RUNTIME_vim ?= "vim"


PACKAGES = " \
    packagegroup-hkiosk-base \
    packagegroup-hkiosk-base-core \
    packagegroup-hkiosk-base-network \
    packagegroup-hkiosk-base-python3 \
"

# TODO include systemd-analyze only when debugging is enabled

RDEPENDS:packagegroup-hkiosk-base = " \
    packagegroup-hkiosk-base-core \
    packagegroup-hkiosk-base-network \
    packagegroup-hkiosk-base-python3 \
    cpufrequtils \
    evtest \
    hkiosk-pmstated \
    hkiosk-set-hostname \
    hkiosk-set-network \
    hkiosk-set-password \
    hkiosk-set-wgquick \
    hkiosk-wakeup-suspend \
    libcec \
    os-release \
    pulseaudio \
    pulseaudio-server \
    systemd-analyze \
    swupdate \
    u-boot-fw-utils \
    usbutils \
    v4l-utils \
"

# TODO include only specific modules

RDEPENDS:packagegroup-hkiosk-base-core = " \
    kernel-modules \
    bash \
    bind-utils \
    bzip2 \
    cpio \
    cracklib \
    ethtool \
    e2fsprogs \
    file \
    findutils \
    grep \
    gzip \
    htop \
    iotop \
    iproute2 \
    iproute2-ss \
    iputils \
    kmod \
    less \
    lsof \
    makedevs \
    module-init-tools \
    openssl \
    procps \
    psmisc \
    rsync \
    screen \
    sed \
    shadow \
    smem \
    sudo \
    tar \
    time \
    unzip \
    util-linux \
    ${VIRTUAL-RUNTIME_vim} \
    wget \
    which \
    xz \
"

RDEPENDS:packagegroup-hkiosk-base-network = " \
    wireguard-tools \
"

RDEPENDS:packagegroup-hkiosk-base-python3 = " \
    python3 \
    python3-evdev \
    python3-pyserial \
    ${VIRTUAL-RUNTIME_python3-gpio} \
"
