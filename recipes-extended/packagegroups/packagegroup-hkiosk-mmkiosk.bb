SUMMARY = "mmKiosk variant"
LICENSE = "MIT"

PR = "r0"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS:packagegroup-hkiosk-mmkiosk = " \
    gstreamer1.0 \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-python \
    hkiosk-run-video \
"
