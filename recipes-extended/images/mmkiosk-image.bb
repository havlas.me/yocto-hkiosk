LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

IMAGE_FEATURES += "ssh-server-openssh read-only-rootfs overlayfs-etc"

IMAGE_INSTALL = " \
    packagegroup-core-boot \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    packagegroup-hkiosk-base \
    packagegroup-hkiosk-mmkiosk \
"

OVERLAYFS_ETC_MOUNT_POINT = "/overlayfs"
OVERLAYFS_ETC_USE_ORIG_INIT_NAME = "0"
OVERLAYFS_ETC_MOUNT_OPTIONS = "defaults,noatime,nodev,noexec,nosuid"
OVERLAYFS_ETC_INIT_TEMPLATE = "${HKIOSKBASE}/recipes-extended/images/mmkiosk-image/overlayfs-etc-preinit.sh.in"

IMAGE_ROOTFS_SIZE = "2621440"
IMAGE_ROOTFS_EXTRA_SPACE = "0"
IMAGE_OVERHEAD_FACTOR = "1"

require swupdate-version.inc

ROOTFS_RO_UNNEEDED ?= "update-rc.d base-passwd ${VIRTUAL-RUNTIME_update-alternatives} ${ROOTFS_BOOTSTRAP_INSTALL}"

inherit core-image
