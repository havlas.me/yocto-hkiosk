LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit swupdate

SRC_URI = " \
    file://sw-description \
"

IMAGE_DEPENDS = "mmkiosk-image"

SWUPDATE_IMAGES = "mmkiosk-image"
SWUPDATE_IMAGES_FSTYPES[mmkiosk-image] = ".ext4.gz"
