add_swupdate_image_version () {
    echo "${MACHINE} ${HWREVISION}" > ${IMAGE_ROOTFS}/etc/hwrevision
    echo "rootfs ${BUILD_ID}" > ${IMAGE_ROOTFS}/etc/sw-version
}

ROOTFS_POSTPROCESS_COMMAND += "add_swupdate_image_version; "
