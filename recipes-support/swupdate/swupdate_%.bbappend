FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
    file://swupdate.cfg \
    file://swupdate.url \
    file://40-swupdate-download \
"

do_install:append () {
    install -m 644 ${WORKDIR}/swupdate.cfg ${D}${sysconfdir}/swupdate.cfg
    install -m 644 ${WORKDIR}/swupdate.url ${D}${sysconfdir}/swupdate.url
    install -m 644 ${SWUPDATE_PUBLIC_KEY} ${D}${sysconfdir}/swupdate.pem

    install -m 644 ${WORKDIR}/40-swupdate-download ${D}${libdir}/swupdate/conf.d/
}

FILES:${PN}:append = " \
    ${sysconfdir}/swupdate.cfg \
    ${sysconfdir}/swupdate.url \
    ${sysconfdir}/swupdate.pem \
    ${libdir}/swupdate/conf.d/40-swupdate-download \
"
