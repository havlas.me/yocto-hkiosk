FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://vimrc"

PACKAGECONFIG ??= " \
    ${@bb.utils.filter('DISTRO_FEATURES', 'acl selinux', d)} \
    nls \
"

do_install:append() {
    install -m 644 ${WORKDIR}/vimrc ${D}/${datadir}/${BPN}/vimrc
}
