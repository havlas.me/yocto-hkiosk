OS_RELEASE_FIELDS = " \
    PRETTY_NAME NAME ID VERSION VERSION_ID BUILD_ID \
    ${@'HOME_URL' if 'HOME_URL' in d else ''} \
    ${@'SUPPORT_URL' if 'SUPPORT_URL' in d else ''} \
    ${@'BUG_REPORT_URL' if 'BUG_REPORT_URL' in d else ''} \
"
OS_RELEASE_UNQUOTED_FIELDS:append = " BUILD_ID"

VERSION = "${DISTRO_VERSION}"
BUILD_ID = "${@time.strftime('%Y%m%d%H%M%S',time.localtime(int(REPRODUCIBLE_TIMESTAMP_ROOTFS)))}"
#VARIANT_ID = "${IMAGE_VARIANT}"
