FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

PACKAGECONFIG ??= " \
    ${@bb.utils.filter('DISTRO_FEATURES', 'systemd', d)} \
    fullscreen \
"

PACKAGECONFIG[fullscreen] = "--enable-img-fullscreen"
PACKAGECONFIG[startup-msg] = ",--disable-startup-msg"
PACKAGECONFIG[progress-bar] = ",--disable-progress-bar"

SPLASH_IMAGES:append = " file://hkiosk-splash.png;outsuffix=hkiosk"
ALTERNATIVE_PRIORITY:psplash-hkiosk[psplash] = "200"
