FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

dirs1777:remove = "${localstatedir}/volatile/tmp"
dirs755:append = " /overlayfs"
dirs755:remove = "${localstatedir}/backups ${localstatedir}/volatile"
volatiles:remove = "tmp"

do_install:append () {
    ln -snf ../tmp ${D}${localstatedir}/tmp
}
