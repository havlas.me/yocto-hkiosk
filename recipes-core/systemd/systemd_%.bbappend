FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
    file://no-splash.conf \
"

do_install:prepend () {
    install -d ${D}${sysconfdir}/tmpfiles.d
    ln -snf /dev/null ${D}${sysconfdir}/tmpfiles.d/etc.conf
    ln -snf /dev/null ${D}${sysconfdir}/tmpfiles.d/home.conf
    #ln -snf /dev/null ${D}${sysconfdir}/tmpfiles.d/00-create-volatile.conf
}

do_install:append () {
    install -d ${D}${systemd_system_unitdir}/getty@tty1.service.d
    install -m 644 ${WORKDIR}/no-splash.conf ${D}${systemd_system_unitdir}/getty@tty1.service.d/no-splash.conf

    rm -f ${D}${libdir}/systemd/network/*.example
    rm -f ${D}${libdir}/systemd/network/*.network

    rm -rf ${D}${localstatedir}/log/journal

    rm -f ${D}${sysconfdir}/tmpfiles.d/00-create-volatile.conf
}

FILES:${PN} += " \
    ${systemd_system_unitdir}/getty@tty1.service.d \
    ${systemd_system_unitdir}/getty@tty1.service.d/no-splash.conf \
    ${sysconfdir}/tmpfiles.d \
    ${sysconfdir}/tmpfiles.d/etc.conf \
    ${sysconfdir}/tmpfiles.d/home.conf \
"

RDEPENDS:${PN}:remove = "volatile-binds"
