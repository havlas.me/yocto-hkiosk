FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
   file://usplash.bmp \
"

do_deploy:append () {
    cp ${WORKDIR}/usplash.bmp ${DEPLOYDIR}/${BOOTFILES_DIR_NAME}
}
