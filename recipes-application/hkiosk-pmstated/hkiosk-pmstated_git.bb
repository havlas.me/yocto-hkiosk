SUMMARY = "hKiosk PowerManagement Daemon"
HOMEPAGE = "https://gitlab.com/havlas.me/hkiosk-pmstated/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI = "git://gitlab.com/havlas.me/hkiosk-pmstated.git;protocol=https;branch=main"
SRCREV = "bdf3df940141b41f306c528b6fde68bd6b3bdf4e"

PV = "0.1.0+git${SRCPV}"
S = "${WORKDIR}/git"

REQUIRED_DISTRO_FEATURES += "systemd"

inherit hkiosk systemd features_check

SYSTEMD_SERVICE:${PN} = " \
    hkiosk-pmstated.service \
    hkiosk-pmstated.path \
"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
    DESTDIR=${D} make install

    install -m 644 -o "${HKIOSK_USER_GROUP}" -g "${HKIOSK_USER_GROUP}" /dev/null ${D}${localstatedir}/lib/hkiosk/pmstate
    echo "on" > ${D}${localstatedir}/lib/hkiosk/pmstate
}

FILES:${PN} += " \
    ${sysconfdir}/default/hkiosk-pmstated \
    ${systemd_system_unitdir}/hkiosk-pmstate.service \
    ${systemd_system_unitdir}/hkiosk-pmstate.service.wants \
    ${systemd_system_unitdir}/hkiosk-pmstated.path \
    ${systemd_system_unitdir}/hkiosk-pmstated.service \
    ${libexecdir}/hkiosk-pmstated \
    ${localstatedir}/lib/hkiosk \
    ${localstatedir}/lib/hkiosk/pmstate \
"

DEPENDS += "hkiosk-base"
RDEPENDS:${PN} += "hkiosk-base"
