SUMMARY = "hKiosk Base"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI = "file://sudoers"

inherit hkiosk useradd

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
    install -d ${D}${localstatedir}/lib/hkiosk
    install -d -o ${HKIOSK_USER_GROUP} -g ${HKIOSK_USER_GROUP} ${D}${localstatedir}/lib/hkiosk/.localstate

    install -d -m 750 ${D}${sysconfdir}/sudoers.d
    install -m 440 ${WORKDIR}/sudoers ${D}${sysconfdir}/sudoers.d/hkiosk
    sed -i -e "s,@USER@,${HKIOSK_USER_GROUP},g" ${D}${sysconfdir}/sudoers.d/hkiosk
}

FILES:${PN} += " \
    ${localstatedir}/lib/hkiosk \
    ${localstatedir}/lib/hkiosk/.localstate \
    ${sysconfdir}/sudoers.d/hkiosk \
"

USERADD_PACKAGES = "${PN}"
USERADD_PARAM:${PN} = " \
    --home /var/lib/hkiosk/.localstate \
    --no-create-home \
    --shell /bin/bash \
    --user-group --uid 1000 ${HKIOSK_USER_GROUP} \
"
