SUMMARY = "hKiosk Set Network Service"
HOMEPAGE = "https://gitlab.com/havlas.me/hkiosk-set-network/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI = "git://gitlab.com/havlas.me/hkiosk-set-network.git;protocol=https;branch=main"
SRCREV = "369272e7c23df92454bdc4ed4b4ab6828bcf17e6"

PV = "0.1.0+git${SRCPV}"
S = "${WORKDIR}/git"

REQUIRED_DISTRO_FEATURES += "systemd"

inherit hkiosk systemd features_check

SYSTEMD_SERVICE:${PN} += "hkiosk-reload-network.path"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
    DESTDIR=${D} make install
}

FILES:${PN} += " \
    ${libexecdir}/hkiosk-set-network \
    ${systemd_system_unitdir}/hkiosk-reload-network.service \
    ${systemd_system_unitdir}/hkiosk-reload-network.path \
    ${systemd_system_unitdir}/hkiosk-set-network@.service \
    ${systemd_system_unitdir}/hkiosk-set-network@.path \
"
