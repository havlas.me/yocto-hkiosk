SUMMARY = "hKiosk Set WireGuard Service"
HOMEPAGE = "https://gitlab.com/havlas.me/hkiosk-set-wgquick/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI = "git://gitlab.com/havlas.me/hkiosk-set-wgquick.git;protocol=https;branch=main"
SRCREV = "60f6425f613c951da5cdfa215965c2b2c8a59e69"

PV = "0.1.0+git${SRCPV}"
S = "${WORKDIR}/git"

REQUIRED_DISTRO_FEATURES += "systemd"

inherit hkiosk systemd features_check

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
    DESTDIR=${D} make install
}

FILES:${PN} += " \
    ${libexecdir}/hkiosk-set-wgquick \
    ${systemd_system_unitdir}/hkiosk-restart-wgquick@.service \
    ${systemd_system_unitdir}/hkiosk-restart-wgquick@.path \
    ${systemd_system_unitdir}/hkiosk-set-wgquick@.service \
    ${systemd_system_unitdir}/hkiosk-set-wgquick@.path \
"
