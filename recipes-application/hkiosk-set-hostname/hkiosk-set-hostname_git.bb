SUMMARY = "hKiosk Set Hostname Service"
HOMEPAGE = "https://gitlab.com/havlas.me/hkiosk-set-hostname/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI = "git://gitlab.com/havlas.me/hkiosk-set-hostname.git;protocol=https;branch=main"
SRCREV = "3370076fe228188bd66d76a63b2d7476b13e292c"

PV = "0.1.0+git${SRCPV}"
S = "${WORKDIR}/git"

REQUIRED_DISTRO_FEATURES += "systemd"

inherit hkiosk systemd features_check

SYSTEMD_SERVICE:${PN} = " \
    hkiosk-set-hostname.service \
    hkiosk-set-hostname.path \
"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
    DESTDIR=${D} make install
}

FILES:${PN} += " \
    ${libexecdir}/hkiosk-set-hostname \
    ${systemd_system_unitdir}/hkiosk-set-hostname.service \
    ${systemd_system_unitdir}/hkiosk-set-hostname.path \
"
