SUMMARY = "hKiosk Set Password Service"
HOMEPAGE = "https://gitlab.com/havlas.me/hkiosk-set-password/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI = "git://gitlab.com/havlas.me/hkiosk-set-password.git;protocol=https;branch=main"
SRCREV = "4fc42e67ac85d7f13eac825db53fa51911c1f09e"

PV = "0.1.0+git${SRCPV}"
S = "${WORKDIR}/git"

REQUIRED_DISTRO_FEATURES += "systemd"

inherit hkiosk systemd features_check

SYSTEMD_SERVICE:${PN} = " \
    hkiosk-set-password.service \
    hkiosk-cleanup-password.service \
"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
    DESTDIR=${D} make install
}

FILES:${PN} += " \
    ${libexecdir}/hkiosk-set-password \
    ${systemd_system_unitdir}/hkiosk-set-password.service \
    ${systemd_system_unitdir}/hkiosk-cleanup-password.service \
"
