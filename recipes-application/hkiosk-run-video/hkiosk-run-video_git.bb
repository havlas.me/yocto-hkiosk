SUMMARY = "hKiosk RunVideo Daemon"
HOMEPAGE = "https://gitlab.com/havlas.me/hkiosk-run-video/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI = "git://gitlab.com/havlas.me/hkiosk-run-video.git;protocol=https;branch=main"
SRCREV = "9f98018825e1b47f179ed409f868d91ca35a3246"

PV = "0.1.0+git${SRCPV}"
S = "${WORKDIR}/git"

REQUIRED_DISTRO_FEATURES += "systemd"

inherit hkiosk systemd useradd features_check

SYSTEMD_SERVICE:${PN} = "hkiosk-run-video.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
    install -d ${D}${libexecdir}
    install -m 755 ${S}/libexec/hkiosk-run-video ${D}${libexecdir}/hkiosk-run-video
    install -d ${D}${datadir}/hkiosk
    install -m 644 ${S}/resources/hkiosk.png ${D}${datadir}/hkiosk/hkiosk.png

    install -d ${D}${sysconfdir}/default
    install -m 644 ${S}/default/hkiosk-run-video ${D}${sysconfdir}/default/hkiosk-run-video
    install -d ${D}${systemd_system_unitdir}
    install -m 644 ${S}/systemd/hkiosk-run-video.service ${D}${systemd_system_unitdir}/hkiosk-run-video.service
    install -d ${D}${systemd_system_unitdir}/hkiosk-run-video.service.requires
    ln -snf ${systemd_system_unitdir}/pulseaudio@.service ${D}${systemd_system_unitdir}/hkiosk-run-video.service.requires/pulseaudio@${HKIOSK_USER_GROUP}.service
}

FILES:${PN} += " \
    ${libexecdir}/hkiosk-run-video \
    ${datadir}/hkiosk \
    ${datadir}/hkiosk/hkiosk.png \
    ${sysconfdir}/default/hkiosk-run-video \
    ${systemd_system_unitdir}/hkiosk-run-video.service \
    ${systemd_system_unitdir}/hkiosk-run-video.service.requires \
    ${systemd_system_unitdir}/hkiosk-run-video.service.requires/pulseaudio@${HKIOSK_USER_GROUP}.service \
"

USERADD_PACKAGES = "${PN}"
GROUPMEMS_PARAM:${PN} = " \
    --group video --add ${HKIOSK_USER_GROUP}; \
    --group audio --add ${HKIOSK_USER_GROUP} \
"

DEPENDS += "hkiosk-base"
RDEPENDS:${PN} = "hkiosk-base"
