SUMMARY = "hKiosk Wakeup/Suspend Daemon"
HOMEPAGE = "https://gitlab.com/havlas.me/hkiosk-wakeup-suspend/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI = "git://gitlab.com/havlas.me/hkiosk-wakeup-suspend.git;protocol=https;branch=main"
SRCREV = "c2ecd45c83c89b32f000b48a3ef8947da70b9504"

PV = "0.1.0+git${SRCPV}"
S = "${WORKDIR}/git"

REQUIRED_DISTRO_FEATURES += "systemd"

inherit hkiosk systemd features_check

SYSTEMD_SERVICE:${PN} = "hkiosk-wakeup-suspend.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
    DESTDIR=${D} make install

    rm -rf ${D}${datadir}

    install -m 644 ${S}/support/cecctl-suspend.sh ${D}${libexecdir}/hkiosk-suspend.d/40-cecctl-suspend.sh
    install -m 644 ${S}/support/cpufreq-suspend.sh ${D}${libexecdir}/hkiosk-suspend.d/90-cpufreq-suspend.sh
    install -m 644 ${S}/support/cpufreq-wakeup.sh ${D}${libexecdir}/hkiosk-wakeup.d/10-cpufreq-wakeup.sh
    install -m 644 ${S}/support/cecctl-wakeup.sh ${D}${libexecdir}/hkiosk-wakeup.d/40-cecctl-wakeup.sh

    sed -i -n -e '/^#\?'"CEC_OSD_NAME"'/!p' -e '$a'"CEC_OSD_NAME=\"${DISTRO_NAME}\"" \
        ${D}${sysconfdir}/default/hkiosk-wakeup-suspend
}

FILES:${PN} += " \
    ${libexecdir}/hkiosk-suspend \
    ${libexecdir}/hkiosk-suspend.d \
    ${libexecdir}/hkiosk-suspend.d/40-cecctl-suspend.sh \
    ${libexecdir}/hkiosk-suspend.d/90-cpufreq-suspend.sh \
    ${libexecdir}/hkiosk-wakeup \
    ${libexecdir}/hkiosk-wakeup.d \
    ${libexecdir}/hkiosk-wakeup.d/10-cpufreq-wakeup.sh \
    ${libexecdir}/hkiosk-wakeup.d/40-cecctl-wakeup.sh \
    ${sysconfdir}/default/hkiosk-wakeup-suspend \
    ${systemd_system_unitdir}/hkiosk-wakeup-suspend.service \
"

DEPENDS += "hkiosk-base"
RDEPENDS:${PN} = "hkiosk-base"
