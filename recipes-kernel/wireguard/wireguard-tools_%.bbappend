FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
    file://wgquick-condition.conf \
"

do_install:append () {
    install -d ${D}${systemd_system_unitdir}/wg-quick@.service.d
    install -m 644 ${WORKDIR}/wgquick-condition.conf ${D}${systemd_system_unitdir}/wg-quick@.service.d/condition.conf
}

FILES:${PN}-wg-quick += " \
    ${systemd_system_unitdir}/wg-quick@.service.d \
    ${systemd_system_unitdir}/wg-quick@.service.d/condition.conf \
"
